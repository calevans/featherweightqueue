# Featherweight Queue Server<br />
Cal Evans <cal@calevans.com>

This is an optional piece of Featherweight Queue, a REST server that can be used to manage jobs and queues.

FeatherweightQueue + THIS + Transport = You can use REST calls to manage the jobs and queues

# Installation

These instructions assume you have a working web server. By defauly, installing this package copeis the needed files into public_html

1. Install `eicc/featherweight-queue`
1. Install and then properly configure a transport. Each Transport's README will contain configuration instructions.
1. Install this package
1. Configure everything
