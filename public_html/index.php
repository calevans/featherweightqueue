<?php

/**
 * This is the entry point for the rest API
 *
 * Routes Defined:
 *   + POST /jobs
 *   + GET /jobs
 *   + GET /jobs/{id}
 *   + DELETE /jobs/{id}
 *   + GET /jobs/count
 *
 *   + GET /queues
 *   + GET /queues/{queueName}
 *   + GET /queues/{queueName}/jobs
 *
 *   - GET /ping < - heath check
 *   - GET /routes < - List the routes defined
 *
 * @todo add basic auth
 */

use Eicc\Fwq\Models\Job;

use Steampixel\Route;

$container = require_once '../app/bootstrap.php';

 /*
  * This is here so Lando won't error out
  */
Route::add('/', function () {
       header('HTTP/1.0 200 Ok');
       return "\nEverything is awesome!\n";
}, 'get');

/*
 * Add a job to the system
 */
Route::add('/jobs', function () use ($container) {
  // accept a job
       header('HTTP/1.0 201 Created');
       header('Accept: application/json');
  // everything is in $_POST
       $jsonEncodedLuw = file_get_contents('php://input');
       $luw = json_decode($jsonEncodedLuw);
       $job = new Job($luw, $container);
       $job->queue();
}, 'post');

/*
 * Get a paginated list of all jobs in the system
 * THIS WILL NEVER BE USED. use /queues instead.
 */
Route::add('/jobs', function () {
  // list all jobs
  // paginated? check for parameters?
       header('HTTP/1.0 200 Ok');
}, 'get');

/*
 * If you know the jobId then you can retrieve a specific job.
 */
Route::add('/jobs/([0-9]*)', function ($jobId) {
       header('HTTP/1.0 418 Not yet implemented');
}, 'get');

/*
 * If you know the jobId then you can delete a specific job.
 */
Route::add('/jobs/([0-9]*)', function ($jobId) {
       header('HTTP/1.0 418 Not yet implemented');
}, 'delete');

/*
 * Get the number of jobs currently queued in the system.
 */
Route::add('/jobs/count', function () {
       header('HTTP/1.0 418 Not yet implemented');
       $transport = $container['transport'];
       return $transport->jobCount();
}, 'get');


/*
 * List the queues in the system
 */
Route::add('/queues', function () use ($container) {
  // accept a job
       header('HTTP/1.0 200 Ok');
       header('Accept: application/json');
       return json_encode($container['transport']->listQueues());
}, 'get');


/*
 * Return the meta for a givne queue
 */
Route::add('/queues/([a-z-0-9-_]*)', function ($queueName) use ($container) {
       header('HTTP/1.0 200 Ok');
       header('Accept: application/json');
       return json_encode($container['queue']($container,$queueName)->getMeta($queueName));
}, 'get');


/*
 * Return the number of jobs in a queue
 */
Route::add('/queues/([a-z-0-9-_]*)/jobs', function ($queueName) use ($container) {
  // accept a job
       header('HTTP/1.0 200 Ok');
       header('Accept: application/json');
       $returnValue = new StdClass();
       $returnValue->name = $queueName;
       $returnValue->count = $container['queue']($container,$queueName)->count();
       return json_encode($returnValue);
}, 'get');


/*
 * These methods are not parts of the main system, they are just helper routes.
 */
/*
 * Health check
 */
Route::add('/ping', function () {
       header('HTTP/1.0 200 Ok');
       return "\nEverything is awesome!\n";
}, 'get');


/*
 * Return a list of routs and verbs
 */
Route::add('/routes', function () use ($container) {
  $routes = Route::getAll();

  $returnValue = [];
  foreach($routes as $route) {
    if (! isset($returnValue[$route['expression']])) {
      $returnValue[$route['expression']] = [];
    }

    $returnValue[$route['expression']][] = $route['method'];
  }
  return json_encode($returnValue);
}, 'get');


/*
 * Everything else
 */
Route::pathNotFound(function ($path) {
  // Do not forget to send a status header back to the client
  // The router will not send any headers by default
  // So you will have the full flexibility to handle this case
       header('HTTP/1.0 404 Not Found');
       return  'Error 404 :-(<br>'
             . 'The requested path "' . $path . '" was not found!';
});


/*
 * Run the Router with the given Basepath
 */
Route::run('/');
