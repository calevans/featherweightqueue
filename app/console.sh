#!/usr/bin/env php
<?php
declare(strict_types=1);

use Eicc\Fwq\QueueRunner\Commands\QueueRunnerCommand;
use Eicc\Fwq\QueueRunner\Commands\MakeQueueCommand;
use Eicc\Fwq\QueueRunner\Commands\DestroyQueueCommand;
use Eicc\Fwq\QueueRunner\Commands\UpdateQueueCommand;
use Eicc\Fwq\QueueRunner\Commands\QueryQueueCommand;

use Symfony\Component\Console\Application;



$container = require_once realpath(dirname(__FILE__)) . '/bootstrap.php';
$app = new Application('FeatherweightQueue', '1.0.0');
$app->container = $container;

$app->addCommands(
  [
    new QueueRunnerCommand(),
    new MakeQueueCommand(),
    new DestroyQueueCommand(),
    new UpdateQueueCommand(),
    new QueryQueueCommand(),
  ]
);

$app->run();

