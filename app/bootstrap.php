<?php

declare(strict_types=1);

/*
 * Create the Pimple Container and load it with all the services needed.
 */

use Eicc\Fwq\QueueRunner\Factories\QueueFactory;
use Pimple\Container;
use Dotenv\Dotenv;
use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$appPath = realpath(dirname(__FILE__) . '/../') . '/';
require_once $appPath . 'vendor/autoload.php';

(Dotenv::createImmutable($appPath))->load();
$container = new Container();

/**
 * Transports will add any needed elements to this array.
 */
$container['paths'] = [
  'root' => $appPath,
  'log' => $appPath . $_ENV['LOG_DIR']
];

$container['log'] = function ($c) {
  $log = new Logger('FWQ');
  $log->pushHandler(new StreamHandler($c['paths']['log'] . $_ENV['LOG_NAME'], $_ENV['LOG_LEVEL']));
  return $log;
};

$container['transport'] = function ($c) {
  $transportClass = $_ENV['TRANSPORT_CLASS_NAME'];
  return new $transportClass($c);
};

/*
 * THIS IS NOT A TRADITIONAL SERVICE!
 * As such, it should return a new Queue object each time and not cache it.
 * Because it's not a service, the container is not automatically passed in. You
 * have to pass it in on each call.
 *
 * To call this:
 * $queue = $container['queue']($container, 'low_priority');
 */
$container['queue'] = $container->protect(function (Container $c, string $name) {
       return QueueFactory::get($c, $name);
});


return $container;
